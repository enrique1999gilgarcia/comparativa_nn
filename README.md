 # comparativa_nn

https://gitlab.com/enrique1999gilgarcia/comparativa_nn.git

Trabajo TFG de Enrique Gil Garc�a para la Universidad Francisco de Vitoria. Consiste en realizar una comparativa entre distintas arquitecturas de redes neuronales sobre los mismos datasets para poder ver cuales tienes mejor rendimiento.
Las bases de datos originales son 3:
- MNIST
- Iris Dataset
- Flowers_color_image
Tras realizar el entrenamiento de las redes sobre estos datasets, podremos concluir cuales son las mejores arquitecturas para dichos datasets.
Se plantea usar tanto arquitecturas state of the art como ResNet18, como arquitecturas creadas desde 0 y optimizadas.
Se pretende logear lo siguiente:
- Tiempo de ejecuci�n
- Diferencia con cambio en train/test ratio.
- Accuracy
- Precision

Todo esto quedar� reflejado en un documento final que se adjuntar� al tribunal para su valoraci�n, tambi�n estar� disponible en este repositorio.

